# Breakfast Wrap McD Ala-Ala

Terinspirasi pas waktu pagi-pagi ngejer travel di Pasteur. 

## Bahan-bahan yang dibutuhkan: 

- Smoked beef (cari yang halal ya...) 
- Kulit tortilla 
- Telur 
- Tomat
- Bawang bombay 
- Merica 
- Minyak goreng
- Mayonaise (Kewpie favorit wkwkwk) 
- Parsley kering 
- Saus sambal/tomat 

## Cara membuat 

1. Panaskan pan.
2. Goreng sebentar smoked beef hingga matang, kemudian sisihkan. 
3. Masukkan bawang bombay yang sudah diiris tipis ke pan. 
4. Kemudian masukkan juga telur untuk dibuat urak-arik sebentar.
5. Tambahkan garam dan merica secukupnya, kemudian angkat dan sisihkan.
6. Panaskan kulit tortilla sebentar. 
7. Kemudian tortilla di letakkan di piring. 
8. Susun smoked beef, telor urak-arik, bawang bombay dan tomat. 
9. Taburi mayonaise dan saus sambal/tomat. 
10. Gulung kulit tortilla dengan rapi. 
11. Taburkan sedikit parsley kering. 
12. Breakfast wrap ala-ala siap dihidangkan.
13. Baca doa sebelum makan. 

TERIMA KASIH.

# Referensi 

[Referensi 1](https://www.basoyen.com/resep-breakfast-wrap-sausage/)
[Referensi 2](https://amandachastity.mycakeprojects.com/resep-sausage-wrap/)

